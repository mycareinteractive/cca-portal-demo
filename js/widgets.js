/*
/*
 * Widgets are small navigation modules that can adapt in anywhere.
 * Widgets only defines behavior, not appearance.
 */
window.nova = window.nova || {};

(function () {

    /*
     * Widget base class
     */
    window.nova.Widget = Class.extend({

        id: null,
        $el: null,  // cached jquery object
        itemSelector: 'a',

        attached: function () {
        },

        detaching: function () {
        },
        /**
         * Overwrite to process link focus
         */
        focus: function ($jqobj) {
            if ($jqobj && $jqobj.length > 0) {
                //anything in lazy group will be "selected" instead of "active"
                if ($jqobj.parent().hasClass('lazy'))
                    $jqobj.addClass('selected');
                else
                    $jqobj.addClass('active');
            }
        },

        /**
         * Overwrite to process link blur
         */
        blur: function (all) {
            if (all)
                this.$(this.itemSelector).removeClass('selected active');
            else
                this.$('*').not('.lazy').find(this.itemSelector).removeClass('selected active');
        },

        /**
         * Overwrite to process clicks
         */
        click: function ($obj) {
            return false;
        },

        /**
         * Overwrite to process keys
         */
        navigate: function (key) {
            return false;
        },

        $: function (selector) {
            return this.$el.find(selector);
        },

        attach: function (obj) {
            $obj = (obj instanceof jQuery) ? obj : $(obj);
            this.id = $obj.attr('id');
            this.$el = $obj;
            if ($obj.length > 0) {
                this._addKeyListener();
                this._addMouseListener();
                this.attached();
            }
        },

        detach: function () {
            this.detaching();
            this._removeMouseListener();
            this._removeKeyListener();
            this.id = null;
            this.$el = $();
        },

        _addKeyListener: function () {
            this.original_keypressed = window.keypressed;
            var context = this;
            window.keypressed = function (keyCode) {
                var event = {};
                var processed = context._keypressed(keyCode, event);
                if (!processed && context.original_keypressed && !event.stopBubble)
                    return context.original_keypressed(keyCode);
                return processed;
            };
        },

        _removeKeyListener: function () {
            window.keypressed = this.original_keypressed;
        },


        _keypressed: function (keyCode, e) {
            var akey = getkeys(keyCode);
            var $curr = this.$(this.itemSelector).filter('.active');
            var $curr2 = this.$(this.itemSelector).filter('.selected');

            var handled = this.navigate(akey, e);
            if (window['msg'])
                msg('( #' + this.id + ' ) keypress: code = ' + keyCode + ' - ' + akey + ' handled:' + handled);

            // fall back to original handler if we ignore it

            //alert(!handled + ' ' + !e.stopBubble + ' ' + $curr.length);

            if (!handled && !e.stopBubble) {
                msg($curr.length);
                if (akey == 'ENTER' && $curr.length > 0) {  // default link click
                    this.blur();
                    this.focus($curr);
                    //return this.click($curr);
                    $curr.click();
                }
                else if (akey == 'ENTER' && $curr2.length > 0) {  // default link click
                    $curr2.click();
                }

                return false;
            }
            else {
                return handled;
            }

            return false;
        },

        _addMouseListener: function () {
            var context = this;
            this.$el.delegate(this.itemSelector, 'click', function (e) {
                context.blur();
                context.focus($(this));
                var processed = context.click($(this));
                e.preventDefault();
            });
        },

        _removeMouseListener: function () {
            this.$el.undelegate(this.itemSelector, 'click');
        },

        _openLink: function ($obj) {

        },
    });

    /*
     * Button Menu
     * Most common widget where a list of buttons link to other pages
     */
    window.nova.ButtonMenu = window.nova.Widget.extend({
        attached: function () {
			if(this.$el.hasClass('autofocus'))  {
                this.$('.menu-group-button').first().click();			
			} 
        },

        click: function ($obj) {
            if ($obj.hasClass('back-button') && window.settings.design) {
                window.history.back();
                return true;
            }
            return false;
        },

        navigate: function (key) {
            var navkeys = ['UP', 'DOWN', 'LEFT', 'RIGHT'];
            var keyIndex = navkeys.indexOf(key);
            if (keyIndex == -1)
                return false;

            var processed = false;
            var $focusedLink = this.$('.active');
            var $nextLink = $();

            if ($focusedLink.length < 1) {   // major menu not selected, select it
                this.focus(this.$(this.itemSelector).first());
                return processed;
            }

            processed = true;

            if (keyIndex < 2 && !this.$el.hasClass('horizontal')) { // UP and DOWN
                if (key == 'UP') {
                    if ($focusedLink.is(':first-child'))
                        $nextLink = $focusedLink.siblings(this.itemSelector).filter(':last');
                    else
                        $nextLink = $focusedLink.prevAll(this.itemSelector).last();
                }
                else if (key == 'DOWN') {
                    if ($focusedLink.is(':last-child'))
                        $nextLink = $focusedLink.siblings(this.itemSelector).filter(':first');
                    else
                        $nextLink = $focusedLink.nextAll(this.itemSelector).first();
                }
                this.blur(true);
                this.focus($nextLink);
            }
            else if (keyIndex >= 2 && this.$el.hasClass('horizontal')) { // LEFT and RIGHT
                if (key == 'LEFT') {
                    if ($focusedLink.is(':first-child'))
                        $nextLink = $focusedLink.siblings(this.itemSelector).filter(':last');
                    else
                        $nextLink = $focusedLink.prevAll(this.itemSelector).last();
                }
                else if (key == 'RIGHT') {
                    if ($focusedLink.is(':last-child'))
                        $nextLink = $focusedLink.siblings(this.itemSelector).filter(':first');
                    else
                        $nextLink = $focusedLink.nextAll(this.itemSelector).first();
                }
                this.blur(true);
                this.focus($nextLink);
            }

            return processed;
        }
    });

    /*
     * Dropdown Menu
     * Used for dual-level menu with fixed number of selections
     */
    window.nova.DropdownMenu = window.nova.Widget.extend({
        attached: function () {
			var major = sessionStorage.getItem('home.htmlmajorprimary');
			var minor = sessionStorage.getItem('home.htmlminorprimary');
		    
			if(this.$el.hasClass('autofocus') && major == null)  {
                this.$('.menu1').click();			
				this.$('.major #menu1').addClass('selected');
				this.$('.minor #menu1-group').show()
				this.$('.minor #menu1-group a').first().addClass('active');								

			} else {
				var filename = window.location.href.substr(window.location.href.lastIndexOf("/")+1);	
				if(filename=='home.html') { 		
					if (parseInt(major) >= 0) {			
						this._focusMajor($('#menu' + major));
                        //this.blur();									
						//var $nthObj = this.$('.major a:nth-child('+parseInt(major)+')')						
						//$nthObj.addClass('selected');									
						//this.$('.minor #menu'+parseInt(major)+'-group').show()
						if (parseInt(minor) >= 0) {
                            this.$('a.active').removeClass('active');
							var $nthObj = this.$('.minor #menu' + major + '-group a:nth-child('+parseInt(minor)+')')						
							$nthObj.addClass('active');
						}
						sessionStorage.clear;					
					}
				}
			}
        },

        click: function ($obj) {
            if ($obj.length <= 0 || $obj.length > 1)
                return false;

            if ($obj.hasClass('menu-group-button')) {
                this._focusMajor($obj);
                return true;
            }
            return false;
        },

        navigate: function (key) {
            var navkeys = ['UP', 'DOWN', 'LEFT', 'RIGHT'];
            var keyIndex = navkeys.indexOf(key);
            if (keyIndex == -1)
                return false;

            var processed = false;
            var $focusedGroup = this.$('.selected');
            var $nextGroup = $();
            var $focusedLink = this.$('.active');
            var $nextLink = $();

            if ($focusedGroup.length < 1) {   // major menu not selected, select it
                this._focusMajor(this.$('.menu-group-button').first());
                return processed;
            }

            processed = true;

            if (keyIndex < 2) { // UP and DOWN
                if ($focusedLink.length < 1) {
                    var groupId = '#' + $focusedGroup.attr('id') + '-group';
                    $nextLink = this.$(groupId).find(this.itemSelector).first();
                }
                else if (key == 'UP') {
                    if ($focusedLink.is(':first-child'))
                        $nextLink = $focusedLink.siblings(':last');
                    else
                        $nextLink = $focusedLink.prev();
                }
                else if (key == 'DOWN') {
                    if ($focusedLink.is(':last-child'))
                        $nextLink = $focusedLink.siblings(':first');
                    else
                        $nextLink = $focusedLink.next();
                }
                this.blur();
                this.focus($nextLink);
            }
            else { // LEFT and RIGHT
                if (key == 'LEFT') {
                    if ($focusedGroup.is(':first-child'))
                        $nextGroup = $focusedGroup.siblings(':last');
                    else
                        $nextGroup = $focusedGroup.prev();
                }
                else if (key == 'RIGHT') {
                    if ($focusedGroup.is(':last-child'))
                        $nextGroup = $focusedGroup.siblings(':first');
                    else
                        $nextGroup = $focusedGroup.next();
                }
                this.blur(true);
                this.focus($nextGroup);
                this.click($nextGroup);
            }


            return processed;
        },

        _focusMajor: function ($obj) {
            if (!$obj || !($obj instanceof jQuery))
                return;
            this.blur(true);
            this.focus($obj);
            var groupId = '#' + $obj.attr('id') + '-group';
            var slideId = '#' + $obj.attr('id') + '-slide';
            this.$('.menu-group').hide();
            this.$(groupId).show();
            this.$('.slide').hide();
            this.$(slideId).show();
			var $nextLink = $();
			$nextLink = this.$(groupId).find('a').first();
			this.focus($nextLink);

        },

        _focusMinor: function ($obj) {

        }
    });


    /*
     * Tab Menu
     * Used for most secondary pages where menu on the left and tabbed content on the right side
     */
    window.nova.TabMenu = window.nova.Widget.extend({
        attached: function () {
            var context = this;

            if (this.$el.hasClass('autofocus'))
                this.$('.menu-tab-button').first().click();

            // if sub-content has more than one page, arrow buttons will show up    
            this.$('.arrow-button').click(function (e) {
                context.click($(this));
            });
        },

        click: function ($obj) {
		
			var path = window.location.pathname;
			var page = path.split("/").pop();
            
			if ($obj.length <= 0 || $obj.length > 1)
                return false;

            if ($obj.hasClass('menu-tab-button')) {
                this._focusMajor($obj);
                return true;
            }
            else if ($obj.hasClass('back-button') && window.settings.design) {
                window.history.back();
                return true;
            }
            else if ($obj.hasClass('sub-page-next')) {
                this._switchPage('RIGHT');
				if(page == 'feedback.html') 
					$obj.removeClass('active');				
						
                return true;
            }
            else if ($obj.hasClass('sub-page-prev')) {
                this._switchPage('LEFT');
				if(page == 'feedback.html') 
					$obj.removeClass('active');				
                return true;
            }
            return false;
        },

        navigate: function (key) {
            var navkeys = ['UP', 'DOWN', 'LEFT', 'RIGHT'];
            var keyIndex = navkeys.indexOf(key);
            if (keyIndex == -1)
                return false;

            var processed = false;
            var $focusedLink = this.$('.selected');
            var $nextLink = $();

            processed = true;

            if (keyIndex < 2) { // UP and DOWN
                if ($focusedLink.length < 1) {
                    $nextLink = this.$('.major').find(this.itemSelector).first();
                }
                else if (key == 'UP') {
                    if ($focusedLink.is(':first-child'))
                        $nextLink = $focusedLink.siblings(':last');
                    else
                        $nextLink = $focusedLink.prev();
                }
                else if (key == 'DOWN') {
                    if ($focusedLink.is(':last-child'))
                        $nextLink = $focusedLink.siblings(':first');
                    else
                        $nextLink = $focusedLink.next();
                }
                this.blur(true);
                this.focus($nextLink);
                if ($nextLink.hasClass('back-button')) {
                    this.$('.menu-tab').hide();
                    this.$('.back-tab').show();
                } else {
                    this.click($nextLink);
                    this.$('.back-tab').hide();

                }
            }
            else { // LEFT and RIGHT

                // If there are buttons, LEFT and RIGHT switch focus between buttons
                // Otherwise they switch focus between pages
                processed = this._switchPoster(key);
                if (!processed)
                    processed = this._switchButton(key);
                if (!processed)
                    processed = this._switchPage(key);


                //alert('left going back' + ' ' + key + ' ' + processed)

                if (!processed && key == 'LEFT') {
                    window.history.back();
                    return true;
                }

            }

            return processed;
        },

        _switchPage: function (key) {
            var $focusedTab = this.$('.minor .menu-tab').filter(':visible');

            if ($focusedTab.length < 1 || $focusedTab.find('.sub-page').length <= 1) { // nothing to slide to
                return false;
            }
            else {
                var $focusedPage = $focusedTab.find('.sub-page').filter(':visible').first();
                var $nextPage = $();
                if (key == 'LEFT') {
                    if ($focusedPage.is(':first-child'))
                        $nextPage = $focusedPage.siblings(':last');
                    else
                        $nextPage = $focusedPage.prev();
                }
                else if (key == 'RIGHT') {
                    if ($focusedPage.is(':last-child'))
                        $nextPage = $focusedPage.siblings(':first');
                    else
                        $nextPage = $focusedPage.next();
                }
                this._focusPage($nextPage);
            }
            return true;
        },

        _switchButton: function (key) {
            var $focusedTab = this.$('.minor .menu-tab').filter(':visible');
            var $focusedPage = $focusedTab.find('.sub-page').filter(':visible').first();
            var $container = ($focusedPage.length > 0) ? $focusedPage : $focusedTab;

            if ($container.length < 1) {
                return false;
            }

            if ($container.find('a').length < 1) {   // no button, let sub-page handle the key
                return false;
            }

            var $focusedButton = $container.find('a.active').first();
            var $nextButton = $();

            if ($focusedButton.length < 1) {
                $nextButton = $container.find('a').filter(':visible').first();
                if ($nextButton.length < 1) {
                    return false;
                }
            }
            else {
                if (key == 'LEFT') {
                    if ($focusedButton.is(':first-child'))
                        $nextButton = $focusedButton.siblings(':last');
                    else
                        $nextButton = $focusedButton.prev();
                }
                else if (key == 'RIGHT') {
                    if ($focusedButton.is(':last-child'))
                        $nextButton = $focusedButton.siblings(':first');
                    else
                        $nextButton = $focusedButton.next();
                }
            }
            this._focusButton($nextButton);

            return true;
        },


        _switchPoster: function (key) {
            var $focusedTab = this.$('.minor .menu-tab').filter(':visible');
            var $focusedPoster = $focusedTab.find('a.active').first();

            var $container = ($focusedPoster.length > 0) ? $focusedPoster : $focusedTab;
            //alert($focusedPoster.length + ' ' + $container.length)	

            if ($container.length < 1) {
                return false;
            }


            var $nextPoster = $();

            if ($focusedPoster.length < 1) {
                $nextPoster = $container.find('a.active').first();

                if ($nextPoster.length < 1) {
                    return false;
                }
            }
            else {

                if (key == 'LEFT') {
                    if ($focusedPoster.is(':first-child'))
                        $nextPoster = $focusedPoster.siblings(':last');
                    else
                        $nextPoster = $focusedPoster.prev();
                }
                else if (key == 'RIGHT') {
                    if ($focusedPoster.is(':last-child'))
                        $nextPoster = $focusedPoster.siblings(':first');
                    else
                        $nextPoster = $focusedPoster.next();
                }
            }
            this._focusPoster($nextPoster);

            return true;
        },


        _focusMajor: function ($menu) {
            if (!$menu || !($menu instanceof jQuery))
                return;
            this.blur(true);
            this.focus($menu);
            var tabId = '#' + $menu.attr('id') + '-tab';

            this._focusTab(this.$(tabId));
        },

        _focusTab: function ($tab) {
            this.$('.menu-tab').hide();
            $tab.show();

            if ($tab.find('.sub-page').length < 1) {
                this.$('.arrow-button').hide();
                this._focusButton($tab.find('a').first());
            }
            else {
                this.$('.arrow-button').show();
                this._focusPage($tab.find('.sub-page').first());
            }
        },

        _focusPage: function ($page) {
            if (!$page || !($page instanceof jQuery))
                return;
            if ($page.length < 1)
                return;

            this.$('.sub-page').hide();
            $page.show();
            this._focusButton($page.find('a').first());

        },

        _focusButton: function ($button) {
            if (!$button || !($button instanceof jQuery))
                return;
            if ($button.length < 1)
                return;

            this.$('a.active').removeClass('active');
            $button.addClass('active');
        },

        _focusPoster: function ($poster) {

            if (!$poster || !($poster instanceof jQuery))
                return;
            if ($poster.length < 1)
                return;

            var title = $poster.find('img').attr("title");

            var $focusedTab = this.$('.minor .menu-tab').filter(':visible');
            $focusedTab.find('a.active').removeClass('active');
            $poster.addClass('active');
            $focusedTab.find('.carouselTitle').html('<p style="display:block">' + title + '</p>');

        }
    });
})();
