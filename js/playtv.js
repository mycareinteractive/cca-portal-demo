$(document).ready(function () {

    playTv(0);
    $(document).keydown(function (e) {
        var key = getkeys(e.which);
        console.log('key ' + key);
        switch (key) {
            case 'ENTER':
            case 'MENU':
            case 'BACK':
            case 'EXIT':
                window.location.href = './home.html';
                break;
            case 'UP':
            case 'CHUP':
                currentChannel++;
                if (currentChannel >= window.channels.length)
                    currentChannel = 0;
                playTv(currentChannel);
                break;
            case 'DOWN':
            case 'CHDN':
                currentChannel--;
                if (currentChannel < 0)
                    currentChannel = window.channels.length - 1;
                playTv(currentChannel);
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                keyChannel(key);
                break;
        }
        return true;
    });
});

$(window).unload(function () {
    stopTv();
});

var currentChannel = 0;
window.settings = window.settings || {};

function stopTv() {
    console.log('Stopping channel...');

    hcap.channel.stopCurrentChannel({
        "onSuccess":function() {
            console.log('Channel stopped.');
        },
        "onFailure":function(f) {
            console.log("onFailure : errorMessage = " + f.errorMessage);
        }
    });
}

function playTv(idx) {
    if (!window.channels) {
        console.log('Channel list is empty, nothing to play.');
        return;
    }

    if (!window.channels[idx]) {
        console.log('Channel index ' + idx + ' out of bound.');
        return;
    }

    currentChannel = idx;
    var ch = window.channels[currentChannel];
    var num = parseInt(ch.Number);

    showLabel(num + ' - ' + ch.Channel, 5000);

    if (window.hcap) {
        console.log('Changing channel to ' + num + ' ...');

        hcap.channel.requestChangeCurrentChannel({
            "channelType": hcap.channel.ChannelType.RF,
            "logicalNumber": num,
            //"ipBroadcastType": hcap.channel.IpBroadcastType.UDP,
            "rfBroadcastType": hcap.channel.RfBroadcastType.CABLE,
            "onSuccess": function () {
                console.log("channel change done.");
            },
            "onFailure": function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });
    }
}

function playTvByNumber(num) {
    for (var i = 0; i < window.channels.length; i++) {
        var ch = window.channels[i];
        if (ch.Number == num) {
            return playTv(i);
        }
    }
}

function showLabel(text, ms) {
    ms = ms || 5000;
    $('#channel-label').show().find('#loadinginfo').text(text);
    $.doTimeout('tv label');
    $.doTimeout('tv label', ms, function () {
        $('#channel-label').hide();
        return false;
    });
}

function keyChannel(key) {
    var cLength = 1;
    $.doTimeout('keycheck');

    var pKey = window.settings.key;

    if (pKey) {
        window.settings.key = '';
        pKey = pKey + key;

        cLength = pKey.length;
        if (cLength == 3) {
            keyChannelDone(pKey);
            return pKey;
        }
        key = pKey;
    }

    if (cLength == 1 && key == '0')
        return;

    showLabel(key, 3000);

    window.settings.key = key;
    $.doTimeout('keycheck', 3000, function () {
        keyChannelDone(window.settings.key);
        return false;
    });
    return key;
}

function keyChannelDone(key) {
    console.log('in keyChannelDone ' + key);
    $.doTimeout('keycheck');
    window.settings.key = '';
    playTvByNumber(key);
    return;
}
