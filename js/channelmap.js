window.channels = [
    {
        "Number": 28,
        "Channel": "ESPN",
        "Multicast": "232.100.0.17",
        "UDP": 10017
    },
    {
        "Number": 31,
        "Channel": "Golf Channel",
        "Multicast": "232.100.0.28",
        "UDP": 10028
    },
    {
        "Number": 34,
        "Channel": "CNN",
        "Multicast": "232.100.0.11",
        "UDP": 10011
    },
    {
        "Number": 38,
        "Channel": "Fox News",
        "Multicast": "232.100.0.24",
        "UDP": 10024
    },
    {
        "Number": 47,
        "Channel": "HGTV",
        "Multicast": "232.100.0.30",
        "UDP": 10030
    },
    {
        "Number": 51,
        "Channel": "A&E",
        "Multicast": "232.100.0.1",
        "UDP": 10001
    },
    {
        "Number": 54,
        "Channel": "Discovery",
        "Multicast": "232.100.0.14",
        "UDP": 10014
    },
    {
        "Number": 67,
        "Channel": "Comedy Central East",
        "Multicast": "232.100.0.12",
        "UDP": 10012
    },
    {
        "Number": 225,
        "Channel": "BBC America",
        "Multicast": "232.100.0.5",
        "UDP": 10005
    },
    {
        "Number": 356,
        "Channel": "Bloomberg",
        "Multicast": "232.100.0.7",
        "UDP": 10007
    },
    {
        "Number": 949,
        "Channel": "Music Choice: Classical Masterpieces",
        "Multicast": "232.100.0.44",
        "UDP": 10044
    }
];