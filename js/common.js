function getkeys(keycode) {
    // notifications
    if(typeof keycode == 'string') {
        var notipos = keycode.indexOf('notification.');
        if(notipos != -1) {
            return keycode.substr(notipos+13);
        }
    }
    
    // button commands
    var keys = Array();
    
    keys[13] = 'ENTER';
    
    keys[27] = 'VIDEO';    // ESC - video fullscreen toggle
    
    keys[33] = 'PGUP';
    keys[34] = 'PGDN';
    keys[35] = 'END';
    keys[36] = 'HOME';
    keys[37] = 'LEFT';
    keys[38] = 'UP';
    keys[39] = 'RIGHT';
    keys[40] = 'DOWN';
    

    //keys[46] = 'POWR';
    
    keys[48] = '0';
    keys[49] = '1';
    keys[50] = '2';
    keys[51] = '3';
    keys[52] = '4';
    keys[53] = '5';
    keys[54] = '6';
    keys[55] = '7';
    keys[56] = '8';
    keys[57] = '9'; 
    
    keys[67] = 'CHDN';     // C key maps to channel-
    keys[88] = 'CHUP';     // X key maps to channel+

    keys[107] = 'VOLU';     // + on numpad
    keys[109] = 'VOLD';     // - on numpad
    
    keys[120] = 'RWND';     // F9
    keys[121] = 'PLAY';     // F10
    keys[122] = 'FFWD';     // F11
    keys[123] = 'STOP';     // F12
    
    keys[216] = 'MENU';
    
    // ******************************************************
    // Enseo codes
    keys[61441] = 'POWR';
    
    keys[61442] = 'UP';
    keys[61443] = 'DOWN';
    keys[61444] = 'LEFT';
    keys[61445] = 'RIGHT';
    keys[61446] = 'ENTER';
    
    keys[61447] = 'MENU';
    keys[61448] = 'VOLD';
    keys[61449] = 'VOLU';
    
    keys[61454] = '1';
    keys[61455] = '2';
    keys[61456] = '3';
    keys[61457] = '4';
    keys[61458] = '5';
    keys[61459] = '6';
    keys[61460] = '7';
    keys[61461] = '8';
    keys[61462] = '9';
    keys[61463] = '0'; 
    
    keys[61464] = 'PLAY';   // play/pause
    keys[61465] = 'STOP';   // stop
    keys[61465] = 'PAUS';   // pause
    keys[61467] = 'RWND';   // rewind
    keys[61468] = 'FFWD';   // fast forward
    keys[61483] = 'CHUP';   // channel+
    keys[61484] = 'CHDN';   // channel-
    keys[61507] = 'POWR';
    keys[61508] = 'OFF';
    keys[61521] = 'CC';     // closed caption
    
    // ******************************************************
    // Barco terminal codes
    keys[16777220] = 'ENTER';   // 0x01000004  RETURN
    keys[16777221] = 'ENTER';   // 0x01000005  ENTER
    keys[16777234] = 'LEFT';    // 0x01000012
    keys[16777235] = 'UP';      // 0x01000013
    keys[16777236] = 'RIGHT';   // 0x01000014
    keys[16777237] = 'DOWN';    // 0x01000015
    
    keys[16777328] = 'VOLD';    // Volume- on terminal panel maps to 0x01000070
    keys[16777329] = 'MUTE';    // Mute on Barco USB keyboard maps to 0x01000071
    keys[16777330] = 'VOLU';    // Volume+ on terminal panel maps to 0x01000072
    
    keys[16777345] = 'STOP';    // 0x01000081
    keys[16777346] = 'RWND';    // 0x01000082
    keys[16777347] = 'FFWD';    // 0x01000083
    keys[16777350] = 'PLAY';    // 0x01000086
    
    keys[16777399] = 'POWR';    // Power button on terminal panel maps to 0x010000b7
    
    keys[17825796] = 'CALL';    // Handset pick up event maps to 0x01100004
    keys[17825797] = 'HANG';    // Handset hang up event maps to 0x01100005
    
    keys[16777216] = 'VIDEO';   // ESC on terminal keyboard
    
    keys[16777272] = 'RWND';    // F9 on terminal keyboard
    keys[16777273] = 'PLAY';    // F10 on terminal keyboard
    keys[16777274] = 'FFWD';    // F11 on terminal keyboard
    keys[16777275] = 'STOP';    // F12 on terminal keyboard
    
    keys[16777459] = 'VIDEO';   // toggle fullscreen video player window 0x010000f3
    
    // ******************************************************
    // special events
    keys['CLOSE'] = 'CLOSE';        
    keys['CLOSEALL'] = 'CLOSEALL';

    // ******************************************************
    // LG codes
    if(window.hcap) {
        keys[461] = 'BACK';
        keys[457] = 'STOP';
        keys[403] = 'RED';
        keys[404] = 'GREEN';
        keys[405] = 'YELLOW';
        keys[406] = 'HOME';
        keys[458] = 'PLAY';
        keys[1001] = 'BACK';
        keys[602] = 'MENU';
        keys[415] = 'PLAY';
        keys[413] = 'STOP';
        keys[19] = 'PAUS';
        keys[417] = 'FFWD';
        keys[412] = 'RWND';
        keys[427] = 'CHUP';
        keys[428] = 'CHDN';
    }
    
    var key = keys[keycode];
    return key;  
}