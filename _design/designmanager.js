jQuery(document).ready(function($){
    
    window.settings = window.settings || {};
    window.settings.design = true;
    initPlatform();
    
    // Initialize widgets
    // dropdown-menu
    var dropdownMenu = new window.nova.DropdownMenu();
    dropdownMenu.attach('.dropdown-menu');
    
    // tab-menu
    var tabMenu = new window.nova.TabMenu();
    tabMenu.attach('.tab-menu');
    
    // hack hash links to open real html pages
    $('a').live('click', function(e){
        openHashLink($(this));
        e.preventDefault();
    });
});
    
var timezone = 'PST8PDT';
var timezoneName = 'America/Los_Angeles';

function openHashLink($obj) {

	var filename = window.location.href.substr(window.location.href.lastIndexOf("/")+1);			
	if(filename == 'home.html') {
			var $major = this.$(".major .selected");

				var $majorList = this.$(".major a");			
			
			var major = $majorList.index($major);
			major = major + 1;
			
			var $minor = this.$(".minor #menu"+major+"-group a.active");
			var $minorList = this.$(".minor #menu"+major+"-group a");
			var minor = $minorList.index($minor);
			minor = minor + 1;
			sessionStorage.setItem(filename + "majorprimary",major);
			sessionStorage.setItem(filename + "minorprimary",minor);
    }

    if(!$obj || $obj.length <= 0)
        return;
    var link = $obj.attr('href');
    if(link.length < 2 || link[0] != '#')
        return;
    var addr = './' + link.substr(1) + '.html';
    window.location.href = addr;
}

function handshake() {
    var appString = "UpCare Design";
    if(window.settings.version == 'ENSEO') {
        Nimbus.handshake(appString);
    }
    else if(window.settings.version == 'NEBULA') {
        Nebula.handshake(appString);
    }
    return;
}

function keyHandler(event) {
    var version = window.settings.version;
    var processed = false;
    var keyCode = null;
    if (version=='ENSEO')   {
        msg('Enseo key received: ' + event.EventMsg + ' type: ' + event.EventType);
        if(event.EventType == Nimbus.Event.TYPE_COMMAND) {
            var Cmd = Nimbus.parseCommand(event.EventMsg);
            msg('Command received: ' + event.EventMsg + ' Actual Value: ' + Cmd.Code);
            processed = keypressed(Cmd.Code);
            keyCode = Cmd.Code;
        }   
        else if (event.EventType == Nimbus.Event.TYPE_NOTIFICATION) {
            msg("Notification received, notification= " + event.EventMsg + ""); 
            /*  
            if (event.EventMsg=='RTSPAnnouncementWaiting')  {
                eofVideo();
            }*/
            keypressed('notification.' + event.EventMsg);                           
            processed = true;
        }
    }
    else if (version=='NEBULA') {
        msg('Nebula key received: ' + event.Code + ' modifiers:' + event.Modifiers);
        processed = keypressed(event.Code);
        keyCode = event.Code;
    }   
    else {
        processed = false;
    }
    
    return processed;
}   

function initPlatform() {
    var version = 'TEST';
    if (typeof window['EONimbus'] != 'undefined') {           
        version = 'ENSEO';      
    } 
    else if (typeof window['EONebula'] != 'undefined') {          
        version = 'NEBULA';     
    } 
    else {      
        version = 'TEST';
    }
    
    window.settings.version = version;
    
    if (version == 'ENSEO') {
        Nimbus.setLogLevel("Nimbus",2);
        Nimbus.setLogLevel("Lib",2);
        Nimbus.setHandshakeTimeout(120);
        Nimbus.addEventListener(keyHandler);
        Nimbus.setTimeZoneMode('Name');
        Nimbus.setTimeZoneName(timezone, timezoneName);
        Nimbus.setConsoleEnabled(true, true);
        Nimbus.setTelnetEnabled(true, true);              
    }
    else if (version == 'NEBULA') {
        Nebula.setHandshakeTimeout(240);
        Nebula.addCommandHandler(keyHandler);
    }
    else if(version == 'TEST') {
        $('body').addClass('test');
    }
    
    if (version != 'ENSEO') { // PC platforms keystrokes
        $(document).keydown(function(e){
            msg('PC keydown received: ' + e.keyCode);
            var handled = keypressed(e.keyCode);
            if(handled) {
                e.preventDefault();
            }
        });
    }
    
    handshake();
    window.setInterval(handshake, 45000);
    msg('Platform "' + version + '" initialized');
}   
    
function msg(message) {
    var version = window.settings.version;
    if (version=='ENSEO') {
        Nimbus.logMessage(message);
    } 
    else {
        if(window.console && window.console.log)
            console.log(message);
    }
}

function keypressed(keyCode)    {
    if(typeof keyCode == 'string') {
        msg('keypress not processed... Actual Value: ' + keyCode);
        return true;    
    }
    var key = getkeys(keyCode);
    
    if (!key)   {
        msg('keypress not interested... Actual Value: ' + keyCode + ' Translated Value: ' + key);
        return false;
    }

    msg('keypress not processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
    return false;
}

function getkeys(keycode) {
    // notifications
    if(typeof keycode == 'string') {
        var notipos = keycode.indexOf('notification.');
        if(notipos != -1) {
            return keycode.substr(notipos+13);
        }
    }
    
    // button commands
    var keys = Array();
    
    keys[13] = 'ENTER';
    
    keys[27] = 'VIDEO';    // ESC - video fullscreen toggle
    
    keys[33] = 'PGUP';
    keys[34] = 'PGDN';
    keys[35] = 'END';
    keys[36] = 'HOME';
    keys[37] = 'LEFT';
    keys[38] = 'UP';
    keys[39] = 'RIGHT';
    keys[40] = 'DOWN';
    

    //keys[46] = 'POWR';
    
    keys[48] = '0';
    keys[49] = '1';
    keys[50] = '2';
    keys[51] = '3';
    keys[52] = '4';
    keys[53] = '5';
    keys[54] = '6';
    keys[55] = '7';
    keys[56] = '8';
    keys[57] = '9'; 
    
    keys[67] = 'CHDN';     // C key maps to channel-
    keys[88] = 'CHUP';     // X key maps to channel+

    keys[107] = 'VOLU';     // + on numpad
    keys[109] = 'VOLD';     // - on numpad
    
    keys[120] = 'RWND';     // F9
    keys[121] = 'PLAY';     // F10
    keys[122] = 'FFWD';     // F11
    keys[123] = 'STOP';     // F12
    
    keys[216] = 'MENU';
    
    // ******************************************************
    // Enseo codes
    keys[61441] = 'POWR';
    
    keys[61442] = 'UP';
    keys[61443] = 'DOWN';
    keys[61444] = 'LEFT';
    keys[61445] = 'RIGHT';
    keys[61446] = 'ENTER';
    
    keys[61447] = 'MENU';
    keys[61448] = 'VOLD';
    keys[61449] = 'VOLU';
    
    keys[61454] = '1';
    keys[61455] = '2';
    keys[61456] = '3';
    keys[61457] = '4';
    keys[61458] = '5';
    keys[61459] = '6';
    keys[61460] = '7';
    keys[61461] = '8';
    keys[61462] = '9';
    keys[61463] = '0'; 
    
    keys[61464] = 'PLAY';   // play/pause
    keys[61465] = 'STOP';   // stop
    keys[61465] = 'PAUS';   // pause
    keys[61467] = 'RWND';   // rewind
    keys[61468] = 'FFWD';   // fast forward
    keys[61483] = 'CHUP';   // channel+
    keys[61484] = 'CHDN';   // channel-
    keys[61507] = 'POWR';
    keys[61508] = 'OFF';
    keys[61521] = 'CC';     // closed caption
    
    // ******************************************************
    // Barco terminal codes
    keys[16777220] = 'ENTER';   // 0x01000004  RETURN
    keys[16777221] = 'ENTER';   // 0x01000005  ENTER
    keys[16777234] = 'LEFT';    // 0x01000012
    keys[16777235] = 'UP';      // 0x01000013
    keys[16777236] = 'RIGHT';   // 0x01000014
    keys[16777237] = 'DOWN';    // 0x01000015
    
    keys[16777328] = 'VOLD';    // Volume- on terminal panel maps to 0x01000070
    keys[16777329] = 'MUTE';    // Mute on Barco USB keyboard maps to 0x01000071
    keys[16777330] = 'VOLU';    // Volume+ on terminal panel maps to 0x01000072
    
    keys[16777345] = 'STOP';    // 0x01000081
    keys[16777346] = 'RWND';    // 0x01000082
    keys[16777347] = 'FFWD';    // 0x01000083
    keys[16777350] = 'PLAY';    // 0x01000086
    
    keys[16777399] = 'POWR';    // Power button on terminal panel maps to 0x010000b7
    
    keys[17825796] = 'CALL';    // Handset pick up event maps to 0x01100004
    keys[17825797] = 'HANG';    // Handset hang up event maps to 0x01100005
    
    keys[16777216] = 'VIDEO';   // ESC on terminal keyboard
    
    keys[16777272] = 'RWND';    // F9 on terminal keyboard
    keys[16777273] = 'PLAY';    // F10 on terminal keyboard
    keys[16777274] = 'FFWD';    // F11 on terminal keyboard
    keys[16777275] = 'STOP';    // F12 on terminal keyboard
    
    keys[16777459] = 'VIDEO';   // toggle fullscreen video player window 0x010000f3
    
    // ******************************************************
    // special events
    keys['CLOSE'] = 'CLOSE';        
    keys['CLOSEALL'] = 'CLOSEALL';
    
    var key = keys[keycode];
    return key;  
}